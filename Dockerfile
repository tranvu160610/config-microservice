#pull base image
FROM openjdk:8-jdk-alpine

#maintainer 
MAINTAINER tranvu160610@gmail.com

#expose port 8080
EXPOSE 8080

#copy config-microservice to docker image
ADD ./target/config-microservice-0.0.1-SNAPSHOT.jar /data/config-microservice-0.0.1-SNAPSHOT.jar

#default command
CMD java -jar /data/config-microservice-0.0.1-SNAPSHOT.jar